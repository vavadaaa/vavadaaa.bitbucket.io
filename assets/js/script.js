$('.pages-casino-slider-main').slick({
    dots: false,
    arrows: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 3000,
});
$('.all-providers_cnt').on('click',function () {
    $('.all-providers_in').toggleClass('all-providers_active_dr')
})


$(function() {
    var Accordion = function(el, multiple) {
        this.el = el || {};
        this.multiple = multiple || false;

        // Variables privadas
        var links = this.el.find('.link');
        // Evento


        links.on('click', {el: this.el, multiple: this.multiple}, this.dropdown)
    }

    Accordion.prototype.dropdown = function(e) {
        var $el = e.data.el;
        $this = $(this),
            $next = $this.next();

        $next.slideToggle();


        if (!e.data.multiple) {

            $el.find('.submenu').not($next).slideUp();
        };

        if(!$this.hasClass('open')){
            $('.link').each(function () {
                $(this).removeClass('open')
            })
            $this.addClass('open')
        }
        else{
            $this.removeClass('open')
        }
    }

    var accordion = new Accordion($('#accordion'), false);
});

$('.footer-lang ul li').on('click',function () {
    $(this).parent().toggleClass('footer-lang_active_dr');
});

